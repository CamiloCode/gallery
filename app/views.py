﻿"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime
from app.forms import *

def home(request):
    """Renders the home page."""
    if request.method == "GET":
        images = Image.objects.all()
        image_form = ImageForm()
        assert isinstance(request, HttpRequest)
        return render(
            request,
            'app/index.html',
            context_instance=RequestContext(
                request,
                {
                    'title': 'Home Page',
                    'year': datetime.now().year,
                    'images': images,
                    'image_form': image_form,
                })
        )
    elif request.method == "POST":
        image_form = ImageForm(request.POST)
        if image_form.is_valid():
            image_form.save()
            images = Image.objects.all()
            image_form = ImageForm()
            assert isinstance(request, HttpRequest)
            return render(
                request,
                'app/index.html',
                context_instance=RequestContext(
                    request,
                    {
                        'title': 'Home Page',
                        'year': datetime.now().year,
                        'images': images,
                        'image_form': image_form,
                    }
                )
            )
        else:
            images = Image.objects.all()
            image_form = ImageForm()
            return render(
                request,
                'app/index.html',
                context_instance=RequestContext(
                    request,
                    {
                        'title': 'Home Page',
                        'year': datetime.now().year,
                        'images': images,
                        'image_form': image_form,
                    }
                )
            )




def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        context_instance = RequestContext(request,
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        })
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        context_instance = RequestContext(request,
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        })
    )